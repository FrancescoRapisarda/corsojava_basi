package corso.lez6.oop.utilizzo;

public class Main {

	public static void main(String[] args) {

//		Persona perUno = new Persona();
//		perUno.setNome("Valeria");
//		perUno.setCognome("Verdi");
//		
//		Persona perDue = new Persona("Giovanni", "Pace");

		Persona giovanni = new Persona("Giovanni", "Pace");
		giovanni.setEta(34);
		giovanni.setTelefono("12345678");
		
		Indirizzo speGiovanni = new Indirizzo("Via le mani dal naso", "AB123", "Roma", 001234, "Roma");
		giovanni.setSpedizione(speGiovanni);
		
//		Indirizzo fatGiovanni = new Indirizzo("Piazza la bomba e scappa", "PZ1234", "Milano", 012341, "MI");
//		giovanni.setFatturazione(fatGiovanni);
		
		giovanni.setFatturazione(new Indirizzo("Piazza la bomba e scappa", "PZ1234", "Milano", 012341, "MI"));
		
//		System.out.println(giovanni.getSpedizione());
//		System.out.println(giovanni.getFatturazione().toString());
		
//		giovanni.stampaPersona();
		giovanni.stampaDettaglio();
		
		
//		int a = 3;
//		int b = 5;
//		int somma = a + b;
//		System.out.println(somma);
//		System.out.println(a + b);
		
		
	}

}
