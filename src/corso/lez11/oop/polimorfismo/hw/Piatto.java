package corso.lez11.oop.polimorfismo.hw;

public class Piatto extends Articolo{

	private boolean isVegano = false;

	public Piatto() {
		
	}
	
	public Piatto(
			String varCodice, 
			String varNome, 
			float varPrezzo, 
			boolean varIsVegano) {
		
		codice = varCodice;
		nome = varNome;
		prezzo = varPrezzo;
		isVegano = varIsVegano;
	}
	
	public boolean isVegano() {
		return isVegano;
	}

	public void setVegano(boolean isVegano) {
		isVegano = isVegano;
	}
	
	public void stampaPiatto() {
		String descrizione = "PIATTO ---------- \n"
				+ "COD: " + codice + "\n"
				+ "NOM: " + nome + "\n"
				+ "PRZ: " + prezzo + "\n";
		
		if(isVegano) {
			descrizione += "Piatto Vegano";
		}
		
		System.out.println(descrizione);
		
		
	}
	
	
	
}
