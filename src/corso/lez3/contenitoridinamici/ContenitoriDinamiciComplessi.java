package corso.lez3.contenitoridinamici;

import java.util.ArrayList;
import java.util.Scanner;

public class ContenitoriDinamiciComplessi {

	public static void main(String[] args) {

//		String[] libro_1 = {"Storia della buonanotte", "Favilli", 	"12345-12345-12345"};
//	    String[] libro_2 = {"Nessuno scrive al Federale", "Vitali", 	"432434-43423-43243"};
//	    String[] libro_3 = {"Buonvino e il caso del bambino scomparso", "Veltroni", "76575-765767-765765"};
//	    String[] libro_4 = {"Storia del Buongiorno", "Favilli", 	"12345-12345-12645"};
//	    String[] libro_5 = {"Il volo del corvo a Roma", "Veltroni", "76575-765867-765765"};
//	    String[] libro_6 = {"Il volo del gabbiano sul tevere", "Veltroni", "76575-765867-765165"};
//	    
//		ArrayList<String[]> libreria = new ArrayList<String[]>();
//		
//		libreria.add(libro_1);
//		libreria.add(libro_2);
//		libreria.add(libro_3);
//		libreria.add(libro_4);
//		libreria.add(libro_5);
//		libreria.add(libro_6);
//		
////		libreria.remove(2);							//Rimozione di un campo dell'ArrayList
//		
//		for(int i=0; i<libreria.size(); i++) {
//			
//			String[] temp = libreria.get(i);
//			
//			String stampa = temp[0] + ", " + temp[1] + " - ISBN: " + temp[2];
//			System.out.println( stampa );
//			
//		}
		
		// ------------------------ INSERIMENTO TRAMITE SCANNER -------------------------------//
		
		/**
		 * Voglio creare un sistema di gestione di libri, le cui funzioni saranno:
		 * 1. Inserisci un nuovo libro in archivio
		 * 2. Stampa tutti i libri che ho in archivio
		 * 3. Elimina un libro dall'archivio
		 */
		
		Scanner interceptor = new Scanner(System.in);
		
		boolean inserimentoAbilitato = true;
		ArrayList<String[]> archivio = new ArrayList<String[]>();
		
		while(inserimentoAbilitato) {
			
			System.out.println("Ciao, benvenuto nel nostro sistema di gestione libri.\n"
					+ "Seleziona la voce del men� che desideri:\n"
					+ "I - Inserisci un nuovo libro\n"
					+ "S - Stampa elenco libri\n"
					+ "E - Elimina un libro in elenco\n"
					+ "Q - Per uscire dal sistema\n");
			
			String comando = interceptor.nextLine();
			
			switch(comando) {
				case "I":
					System.out.println("Inserisci il titolo del Libro:");
					String titolo = interceptor.nextLine();
					System.out.println("Inserisci l'autore del Libro:");
					String autore = interceptor.nextLine();
					System.out.println("Inserisci il codice ISBN del Libro:");
					String isbn = interceptor.nextLine();
					
					String[] libro = { titolo, autore, isbn };			//Creo un Array di Stringhe contenente i dettagli del libro
					archivio.add(libro);								//Aggiungo il libro all'interno dell'ArrayList
					break;
				case "S":
					System.out.println("---------------LISTA-LIBRI-------------");
					
					for(int i=0; i<archivio.size(); i++) {
						String[] temp = archivio.get(i);
						
						String stampaLibro = temp[0] + ", " + temp[1] + " - ISBN: " + temp[2];
						System.out.println(stampaLibro);
					}
					
					System.out.println("---------------------------------------");
					break;
				case "E":

					/*
					 * Titolo 1 - Autore 1 - ISBN1
					 * Titolo 2 - Autore 2 - ISBN2
					 * Titolo 3 - Autore 3 - ISBN3
					 * Titolo 4 - Autore 4 - ISBN4
					 */
					
					System.out.println("Inserisci il codice ISBN da eliminare:");
					String codiceDaEliminare = interceptor.nextLine();
					
					for(int i=0; i<archivio.size(); i++) {
						
						String[] temp = archivio.get(i); 
						
						if(temp[2].equals(codiceDaEliminare)) {
							archivio.remove(i);
						}
					
					}

					break;
				case "Q":
					inserimentoAbilitato = false;
					System.out.println("Grazie per aver utilizzato il nostro sistema!");
					break;
				default:
					System.out.println("Comando non riconosciuto");
			}
			
		}
		
		/**
		 * Creare un sistema di gestione universitaria, in particolare studenti.
		 * Il sistema deve essere in grado di:
		 * 1. Inserire un nuovo studente
		 * 2. Stampare l'elenco degli studenti gi� inseriti
		 * 
		 * Ogni studente sar� caratterizzato da: NOME, COGNOME, MATRICOLA, CORSO DI STUDI
		 * 
		 * Realizzare tutto in maniera interattiva con uno Scanner ed un menu di scelta funzione.
		 */
	}

}
