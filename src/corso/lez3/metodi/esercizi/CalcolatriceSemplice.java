package corso.lez3.metodi.esercizi;

import java.util.Scanner;

public class CalcolatriceSemplice {

	public static void stampa(float numDaStampare) {
		System.out.println(numDaStampare);
	}
	
	public static void somma(float numUno, float numDue) {
		float risultato = numUno + numDue;
		stampa(risultato);
	}
	
	public static void sottrazione(float numUno, float numDue) {
		float risultato = numUno - numDue;
		stampa(risultato);
	}
	
	public static void moltiplicazione(float numUno, float numDue) {
		float risultato = numUno * numDue;
		stampa(risultato);
	}
	
	public static void divisione(float numUno, float numDue) {
		if(numDue == 0) {
			System.out.println("Attenzione, la divisione per zero non � contemplata!");
		}
		else {
			float risultato = numUno / numDue;
			stampa(risultato);
		}
		
	}

	public static void main(String[] args) {

		Scanner interceptor = new Scanner(System.in);
		
		boolean inserimentoAbilitato = true;
		
		while(inserimentoAbilitato) {
			System.out.println("Scegli l'operazione da effettuare:\n"
					+ "SO - Somma\n"
					+ "ST - Sottrazione\n"
					+ "MT - Moltiplicazione\n"
					+ "DI - Divisione\n"
					+ "Q - Quit");
			
			String comando = interceptor.nextLine();
			
			float numeroUno;			//Dichiaro le variabili fuori dallo Switch perch� altrimenti ci sarebbe (ad ogni caso)
			float numeroDue;			//una dupilicazione delle variabili locali.
			
			switch (comando) {
				case "SO":
					System.out.println("Inserisci il primo numero");
					numeroUno = Float.parseFloat(interceptor.nextLine());
					System.out.println("Inserisci il secondo numero");
					numeroDue = Float.parseFloat(interceptor.nextLine());
					
					somma(numeroUno, numeroDue);
					break;
				case "ST":
					System.out.println("Inserisci il primo numero");
					numeroUno = Float.parseFloat(interceptor.nextLine());
					System.out.println("Inserisci il secondo numero");
					numeroDue = Float.parseFloat(interceptor.nextLine());
					
					sottrazione(numeroUno, numeroDue);
					break;
				case "MT":
					System.out.println("Inserisci il primo numero");
					numeroUno = Float.parseFloat(interceptor.nextLine());
					System.out.println("Inserisci il secondo numero");
					numeroDue = Float.parseFloat(interceptor.nextLine());
					
					moltiplicazione(numeroUno, numeroDue);
					break;
				case "DI":
					System.out.println("Inserisci il primo numero");
					numeroUno = Float.parseFloat(interceptor.nextLine());
					System.out.println("Inserisci il secondo numero");
					numeroDue = Float.parseFloat(interceptor.nextLine());
					
					divisione(numeroUno, numeroDue);
					break;
				case "Q":
					inserimentoAbilitato = !inserimentoAbilitato;
					System.out.println("Ciao!");
					break;
				default:
					System.out.println("Comando non riconosciuto!");
					break;
			}
		}
		
		interceptor.close();
		
	}

}
