package corso.lez11.oop.statiche.espansione;

public class Contatori {

	private static int contatore = 0;
	
	public static void incrementaContatore() {
		contatore++;
	}
	
	public static int restituisciContatore() {
		return contatore;
	}
	
}
