package corso.lez1.stringhe;

public class Sottostringhe {

	public static void main(String[] args) {


		/* TIP, come generare un codice con sottostringhe */
		String nome = "Giovanni";
		String cognome = "Pace";
		String dataNascita = "01/01/2000";
		
		String codice = nome.substring(0, 2).toUpperCase() + cognome.substring(0, 2).toUpperCase() + "-" + dataNascita.substring(6);
		
		System.out.println(codice);
								
//		System.out.println(nome.substring(4));		//Sottostringa dalla posizione 4 in poi
//		
//		System.out.println(nome.substring(2, 4));	//Sottostringa compresa tra la posizione 2 e 4
	}

}
