package corso.lez11.oop.polimorfismo.hw;

public class Bevanda extends Articolo{

	private float volume;
	
	public Bevanda() {
		
	}
	
	public Bevanda(
			String varCodice, 
			String varNome, 
			float varPrezzo, 
			float varVolume) {
		
		codice = varCodice;
		nome = varNome;
		prezzo = varPrezzo;
		volume = varVolume;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		volume = volume;
	}
	
	public void stampaBibita() {
		String descrizione = "BEVANDA ---------- \n"
				+ "COD: " + codice + "\n"
				+ "NOM: " + nome + "\n"
				+ "PRZ: " + prezzo + "\n"
				+ "VOL: " + volume;
		
		System.out.println(descrizione);
	}
	
}
