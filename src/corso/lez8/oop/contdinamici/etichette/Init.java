package corso.lez8.oop.contdinamici.etichette;

public class Init {

	public static void main(String[] args) {

		Etichetta et1 = new Etichetta("ET1234", "A", 5);
		String oggettoLetterale = et1.toString();
		System.out.println(oggettoLetterale);

		/*
		 * Si comporta allo stesso modo di sopra perch� accede
		 * direttamente al metodo toString di default!
		 */
//		System.out.println(et1);
		
		//2 mesi dopo
		
		/*
		 * Operazione non possibile perch� l'etichetta viene creata
		 * tramite il costruttore che ne assegna il codice ma non c'�
		 * (l'ho commentato) il metodo set per la modifica!
		 */
//		et1.setCodiceEtichetta("BLABLABLA");
//		System.out.println(et1);
		
		et1 = null;		//Tronco il puntatore perch� ho sbagliato
		et1 = new Etichetta("BLA12345", "A", 5);	//Riassegno il nuovo oggetto etichetta

		System.out.println(et1);
	}

}
