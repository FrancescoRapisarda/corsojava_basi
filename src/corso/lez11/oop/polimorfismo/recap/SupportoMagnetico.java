package corso.lez11.oop.polimorfismo.recap;

public abstract class SupportoMagnetico {
	
	private String codice;
	private String intestatario;
	
	protected String getCodice() {
		return codice;
	}
	protected void setCodice(String codice) {
		this.codice = codice;
	}
	protected String getIntestatario() {
		return intestatario;
	}
	protected void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}
	
	

}
