package corso.lez4.recapmetodi;

public class MetodoConRitornoBooleano {

	/**
	 * Questa funzione verifica se l'et� fornita � maggiore o uguale di 18 anni
	 * @param varEta	Parametro et� sotto forma di numero intero
	 * @return			true se sei maggiorenne - false se sei miniorenne
	 */
	public static boolean verificaMaggiorenne(int varEta) {
		System.out.println("Sono entrato nella funzione");
		
		if(varEta >= 18) {
			System.out.println("Sono entrato nel ramo maggiore di 18 anni");
			return true;
		}
		else {
			System.out.println("Sono entrato nel ramo minore di 18 anni");
			return false;
		}
		
//		System.out.println("Fine della funzione!");		//Il codice non � raggiungibile a causa dei return che avvengono in qualsiasi caso prima di lui!
		
	}
	
	public static void main(String[] args) {

		int eta = 13;
		
		boolean risultato = verificaMaggiorenne(eta);
		
		if(risultato) {
			System.out.println("Sei maggiorenne");
		}
		else {
			System.out.println("Sei minorenne");
		}
		
	}

}
