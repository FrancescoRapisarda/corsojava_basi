package corso.lez3.contenitoridinami.esercizio;

import java.util.ArrayList;
import java.util.Scanner;

public class GestioneStudenti {

	public static void main(String[] args) {

		/**
		 * Creare un sistema di gestione universitaria, in particolare studenti.
		 * Il sistema deve essere in grado di:
		 * 1. Inserire un nuovo studente
		 * 2. Stampare l'elenco degli studenti gi� inseriti
		 * 3. NEW Eliminazione dello studente per matricola
		 * 
		 * Ogni studente sar� caratterizzato da: NOME, COGNOME, MATRICOLA, CORSO DI STUDI
		 * 
		 * Realizzare tutto in maniera interattiva con uno Scanner ed un menu di scelta funzione.
		 */
		
		Scanner interceptor = new Scanner(System.in);
		ArrayList<String[]> elenco = new ArrayList<String[]>();
		
		boolean scritturaAbilitata = true;
		while(scritturaAbilitata) {
			
			System.out.println("---------------------------\n"
					+ "I - Inserisci\n"
					+ "S - Stampa\n"
					+ "E - Eliminazione\n"
					+ "Q - Uscita\n");
			
			String comando = interceptor.nextLine();
			
			switch(comando) {
				case "I":

					System.out.println("Inserisci il nome");
					String nome = interceptor.nextLine();
					System.out.println("Inserisci il cognome");
					String cognome = interceptor.nextLine();
					System.out.println("Inserisci il matricola");
					String matricola = interceptor.nextLine();
					System.out.println("Inserisci il corso di studi");
					String corso = interceptor.nextLine();
					
					String[] studente = { nome, cognome, matricola, corso };
					elenco.add(studente);
					
					break;
				case "S":

					System.out.println("------------------------------");
					for(int i=0; i<elenco.size(); i++) {
						String[] temp = elenco.get(i);
						System.out.println(temp[0] + " " + temp[1] + " - Matr: " + temp[2] + " - Corso: " + temp[3]);
					}
					System.out.println("------------------------------");
					
					break;
				case "E":
					System.out.println("Seleziona la matricola:");
					String matrDaEliminare = interceptor.nextLine();
					
					for(int i=0; i<elenco.size(); i++) {
						String[] temp = elenco.get(i);
						
						if(temp[2].equals(matrDaEliminare)) {
							elenco.remove(i);
						}
					}
					
					break;
				case "Q":
					scritturaAbilitata = !scritturaAbilitata;			//Cambia il valore all'opposto in una variabile booleana
					System.out.println("Addio ;(");
					break;
				default:
					System.out.println("Errore - Comando non riconosciuto");
			}
			
		}
		
		interceptor.close();
		
	}

}
