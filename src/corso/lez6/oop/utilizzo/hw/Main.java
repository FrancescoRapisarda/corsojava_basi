package corso.lez6.oop.utilizzo.hw;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		/**
		 * Creare un sistema in grado di immagazinare i dati relativi ad una persona.
		 * Inoltre, sar� necessario immagazinare, all'interno di una persona, i dati 
		 * relativi a:
		 * - Codice Fiscale
		 * |- CODICE
		 * |_ Data di scadenza
		 * 
		 * - Carta di Identita: 
		 * |- CODICE
		 * |- Data di Emissione
		 * |- Data di Scadenza
		 * |_ Emissione (comune, zecca dello stato)
		 */
		
		Persona giovanni = new Persona("Giovanni", "Pace", "KUHGIUGH", "01/01/2020");
		giovanni.associaCartaIdentita("AB1234", "01/01/2021", "01/01/2020", "ZDS");
		
		
		Persona marika = new Persona("Marika", "Viola");
		CodiceFiscale cfMarika = new CodiceFiscale("MAR", "01/01/2021");
		marika.setCodFis(cfMarika);
		CartaIdentita ciMarika = new CartaIdentita("AB1234", "01/01/2021", "01/01/2020", "ZDS");
		marika.setCarIde(ciMarika);
		
		/*
		 * 1. Crea 4 persone e le inserisci in un ArrayList di persone
		 * 2. Stampa tutti i dettagli di tutte le persone nella maniera pi� efficiente possbile
		 */
		
		ArrayList<Persona> rubrica = new ArrayList<Persona>();
		rubrica.add(marika);
		rubrica.add(giovanni);
	}

}
