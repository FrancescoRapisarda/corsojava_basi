package corso.lez1.variabili;

public class Variabili {

	public static void main(String[] args) {

//		int a;					//Dichiarazione della variabile
//		a = 5;					//Assegnazione della variabile
//		
////		System.out.println(a);	//Output...
//		
//		int b = 10;
////		System.out.println(b);
//		
//		int somma;
//		somma = a + b;
//		
//		System.out.println("La somma delle variabili �: " + somma);
//		
////		System.out.println("Ciao " + "sono " + "Giovanni");				//Concatenazione di stringhe
		
		// --------------------------------------------------- //
		
//		int valoreUno = 6;
//		
//		valoreUno = 18;
//		
////		System.out.println(valoreUno);
////		int valoreUno = 89;					//ERRORE!!! Variabile gi� dichiarata in precedenza!
		
		// --------------------------------------------------- //
		
//		int valUno = 12;
//		int valDue = 8;
//		
////		System.out.println(valUno + valDue);							//20
//		System.out.println("Il risultato �: " + valUno + valDue);		//Il risultato �: 128
//		
//		System.out.println("Il risultato �: " + (valUno + valDue));		//Il risultato �: 20 GIUSTO!!!!!!! :D
		
		// --------------------------------------------------- //

//		int valoreUno = 2147483647;
//		
//		int valoreDue = 100;
//		
//		int somma = valoreUno + valoreDue;			//Vado fuori dal limite di 100 unit�
//		
//		System.out.println(somma);					//Comportamento errato!!!!!!! :(
		
		// --------------------------------------------------- //

//		float valoreFloat = 54.3f;
//		
//		double valoreDouble = 89.1d;
//		
////		System.out.println(valoreFloat + valoreDouble);
//		
//		System.out.println(15 + 23.5f + 1.22d);
//		
		// --------------------------------------------------- //

//		float a = 10.0f;
//		float b = 3.0f;
//		
//		float q = a / b;
//		
//		System.out.println(q);
//		
		
		// --------------------------------------------------- //

		double valoreDb = 155.4f;			//Salvataggio di un Float all'interno di un Double
		valoreDb = 453.1d;
		
	}

}
