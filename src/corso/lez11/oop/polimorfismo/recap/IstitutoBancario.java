package corso.lez11.oop.polimorfismo.recap;

import java.util.ArrayList;

public class IstitutoBancario {

	private String nomeFiliale;
	private String indirizzoFiliale;
	
	private ArrayList<SupportoMagnetico> elencoCarte = new ArrayList<SupportoMagnetico>();

	public IstitutoBancario(String varNomeFiliale, String varIndirizzFiliale) {
		this.nomeFiliale = varNomeFiliale;
		this.indirizzoFiliale = varIndirizzFiliale;
	}
	
	/**
	 * Metodo unico per la creazione di una carta credito/debito all'interno dell'elenco
	 * @param varTipo			CREDITO | DEBITO
	 * @param varCodice
	 * @param varIntestatario
	 * @param varFido
	 * @param varDeposito
	 */
	public void inserisciCarta(String varTipo, String varCodice, String varIntestatario, float varFido, float varDeposito) {
		switch(varTipo) {
			case "CREDITO":
				CartaCredito tempCC = new CartaCredito(varCodice, varIntestatario, varFido);
				elencoCarte.add(tempCC);
				System.out.println("Ho inserito una Carta di CREDITO");
				break;
			case "DEBITO":
				CartaDebito tempCD = new CartaDebito(varCodice, varIntestatario, varDeposito);
				elencoCarte.add(tempCD);
				System.out.println("Ho inserito una Carta di DEBITO");
				break;
			default:
				System.out.println("ERRORE NEL TIPO DI SUPPORTO");
		}
	}
	
	public void contaCarte() {
		
		int contatoreDebito = 0;
		int contatoreCredito = 0;
		
		for(int i=0; i<elencoCarte.size(); i++) {
			
			SupportoMagnetico temp = elencoCarte.get(i);
			
			if(temp instanceof CartaCredito) {		//Verifico che temp sia una Istanza della classe Carta di Credito
				contatoreCredito++;
			}
			
			if(temp instanceof CartaDebito) {
				contatoreDebito++;
			}
		}
		
		System.out.println("Carte Credito: " + contatoreCredito);
		System.out.println("Carte Debito: " + contatoreDebito);
		
	}
	
	public void stampaTutteLeCarte() {
		
		for(int i=0; i<elencoCarte.size(); i++) {
			
			SupportoMagnetico temp = elencoCarte.get(i);		//Binding dinamico dove al RUN TIME decido di che tipo � temp!
			
			if(temp instanceof CartaCredito) {					//Mi assicuro che l'oggetto polimorfico appena incontrato sia effettivamente una Carta di Credito
				CartaCredito ccTemp = (CartaCredito) temp;		//Cast, converto (ed assicuro) che temp sia effettivamente una Carta di Credito
				ccTemp.stampaCartaCredito();					//Dato che ccTemp � a tutti gli effetti una carta di credito, posso accedere a tutti i suoi metodi personalizzati!
			}
			
			if(temp instanceof CartaDebito) {
				CartaDebito cdTemp = (CartaDebito) temp;
				cdTemp.stampaCartaDebito();
			}
			
		}
	}
	
	public String getNomeFiliale() {
		return nomeFiliale;
	}

	public void setNomeFiliale(String nomeFiliale) {
		this.nomeFiliale = nomeFiliale;
	}

	public String getIndirizzoFiliale() {
		return indirizzoFiliale;
	}

	public void setIndirizzoFiliale(String indirizzoFiliale) {
		this.indirizzoFiliale = indirizzoFiliale;
	}
	
	
}
