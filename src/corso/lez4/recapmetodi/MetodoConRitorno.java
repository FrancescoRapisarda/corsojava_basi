package corso.lez4.recapmetodi;

public class MetodoConRitorno {

	public static float elevaAlQuadrato(float varNumero){
		float risultato = varNumero * varNumero;
		return risultato;
	}
	
	public static String formattaMatricola(int varAnno, int varMatricola) {
		String risultato = varAnno + "-" + varMatricola;
		return risultato;
	}
	
	public static void main(String[] args) {

//		float numUno = elevaAlQuadrato(5.0f);
//		System.out.println(numUno);
//		
//		
//		float numDue = elevaAlQuadrato(100.0f);
//		System.out.println(numDue);
//		
//		System.out.println( elevaAlQuadrato(50.0f) );		//Funzione eseguita al Run-time che si comporta come il contenuto di una variabile
		
		
		// ----------------------------------------------- //
		/*
		 * Scrivere un sistema che, dato in input il numero di matricola e l'anno di immatricolazione
		 * mi restituisca in ouput una stringa formattata in questo modo: 2021-123456789	//anno-matricola
		 */
		
		String matricolaFormattataUno = formattaMatricola(2021, 123456789);
		System.out.println(matricolaFormattataUno);
		System.out.println(matricolaFormattataUno);
		System.out.println(matricolaFormattataUno);
		System.out.println(matricolaFormattataUno);
		
		System.out.println( formattaMatricola(2021, 987654321) );
		System.out.println( formattaMatricola(2021, 987654321) );
		System.out.println( formattaMatricola(2021, 987654321) );
		System.out.println( formattaMatricola(2021, 987654321) );
		
	}

}
