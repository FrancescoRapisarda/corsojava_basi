package corso.lez4.oop.intro;

public class Main {

	public static void main(String[] args) {

		//ferrarelle e santaCroce sono ISTANZE (oggetto) della CLASSE Bottiglia
//		Bottiglia ferrarelle = new Bottiglia();
//		ferrarelle.altezza = -30.0f;
//		ferrarelle.raggio = 5.0f;
//		ferrarelle.materiale = "PET";
//		ferrarelle.colore = "Verde";
//		ferrarelle.stampa();
//		System.out.println(ferrarelle.calcolaVolume());
//		
//		Bottiglia santaCroce = new Bottiglia();
//		santaCroce.altezza = 31.0f;
//		santaCroce.raggio = 4.0f;
//		santaCroce.materiale = "PET";
//		santaCroce.colore = "Trasparente";
//		santaCroce.stampa();
//		System.out.println(santaCroce.calcolaVolume());
		
		Bottiglia ferrarelle = new Bottiglia();
		ferrarelle.setAltezza(-30.0f);
		ferrarelle.setRaggio(5.0f);
		ferrarelle.setMateriale("PET");
		ferrarelle.setColore("Verde");
//		ferrarelle.stampa();
//		System.out.println(ferrarelle.calcolaVolume());

//		float altezza = ferrarelle.getAltezza();
//		System.out.println(altezza);
		
		System.out.println(ferrarelle.getAltezza());
		System.out.println(ferrarelle.getRaggio());
		System.out.println(ferrarelle.getMateriale());
		System.out.println(ferrarelle.getColore());
		System.out.println(ferrarelle.getProduttore());
		
	}

}
