package corso.lez6.oop.utilizzo;

public class Indirizzo {

	private String via;
	private String civico;
	private String citta;
	private int cap;
	private String provincia;
	
	public Indirizzo(String varVia, String varCivico, String varCitta, int varCap, String varProvincia) {
		this.via = varVia;
		this.civico = varCivico;
		this.citta = varCitta;
		this.cap = varCap;
		this.provincia = varProvincia;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getCivico() {
		return civico;
	}

	public void setCivico(String civico) {
		this.civico = civico;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public int getCap() {
		return cap;
	}

	public void setCap(int cap) {
		this.cap = cap;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	/**
	 * Metodo che mi restituisce l'oggetto sotto forma di stringa
	 */
	public String toString() {
		String dettagli = this.via + ", " + this.civico + " - " + this.citta + 
				"(" + this.provincia + ") " + this.cap; 	//Via olmo, 43 - Roma(Roma) 00987
		return dettagli;
	}
	
	
	
}
