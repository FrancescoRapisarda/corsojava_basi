package corso.lez1.controllisemplici;

public class ControlliSemplici {

	public static void main(String[] args) {

		//Variabili booleane
//		boolean varBooleana = false;
//		System.out.println(varBooleana);
		
		// --------------------IF ELSE--------------------- //
		
		/*
		 * if(condizione){
		 * 		Blocco di codice se la condizione � verificata
		 * }
		 * else{
		 * 		Blocco di codice se la condizione NON � verificata
		 * }
		 */
		
//		int eta = 16;
		
		/*
		 * OPERATORI DI COMPARAZIONE, restituiscono un Booleano!
		 * a > b
		 * a >= b
		 * a < b
		 * a <= b
		 * a == b		Uguaglianza
		 * a != b		Disuguaglianza
		 */
		
//		if(eta >= 18) {
//			System.out.println("Sei maggiorenne");
//		}
//		else {
//			System.out.println("Sei minorenne");
//		}
		
//		if(eta > 130) {
//			System.out.println("Te li porti veramente bene!");
//		}
//		else {
//			
//			if(eta < 0) {
//				System.out.println("Gli embrioni non sono ammessi!");
//			}
//			else {
//				
//				if(eta >= 18) {
//					System.out.println("Sei maggiorenne");
//				}
//				else {
//					System.out.println("Sei minorenne");
//				}
//				
//			}
//			
//		}
		
//		System.out.println(18 == 18);
		
//		if(false) {										//Ramo del true
//			System.out.println("Ramo del true");
//		}
//		else {											//Ramo del false
//			System.out.println("Ramo del false");
//		}
		
		/*
		 * Scrivere un frammento di programma che mi restituisce se maggiorenne o minorenne stando attenti
		 * a vlore minimo e massimo (0 e 130)
		 */
		
		int eta = 45;
		
		//AND -----> && -----> moltiplicazione
		//    1     x     1			= 1
		//  true   &&   true		= true
		if(eta >= 0 && eta <= 130) {
			if(eta >= 18) {
				System.out.println("Sei maggiorenne");
			}
			else {
				System.out.println("Sei minorenne");
			}
		}
		else {
			System.out.print("Errore");
		}
		

		
		eta = 45;
		
		//OR -----> || -----> somma
		//	0	   ||	0			= 0
		//	false  ||   false		= false
		if(eta < 0 || eta > 130) {
			System.out.print("Errore");
		}
		else {
			if(eta >= 18) {
				System.out.println("Sei maggiorenne");
			}
			else {
				System.out.println("Sei minorenne");
			}
		}
		
		/*
		 * Creare un sistema di controllo degli ingressi al ristorante:
		 * In input avremo la temperatura (salvata in una variabile).
		 * Se la temperatura � maggiore o uguale di 37.5� vietare l'ingresso al ristorante, se la temperatura
		 * � inferiore a 37.5� permettere l'ingresso.
		 * 
		 * Attenzione!!!!!!!! Non vi vuole un medico per dire che al di sotto dei 34� mostrare un errore,
		 * al di sopra dei 42� mostrare lo stesso errore!
		 */
		
		
		
		
	}

}
