package corso.lez11.oop.polimorfismo.recap;

public class CartaCredito extends SupportoMagnetico {

	private float fido = 0;
	
	public CartaCredito() {
		
	}
	
	public CartaCredito(String varCodice, String varIntestatario, float varFido) {
		super.setCodice(varCodice);
		super.setIntestatario(varIntestatario);
		this.fido = varFido;
	}

	public float getFido() {
		return fido;
	}

	public void setFido(float fido) {
		this.fido = fido;
	}
	
	public void stampaCartaCredito() {
		String dettaglio = "DETTAGLIO Carta di Credito: \n"
				+ "Intestatario: " + super.getIntestatario() + "\n"
				+ "Codice: " + super.getCodice() + "\n"
				+ "Fido: " + this.fido;
		
		System.out.println(dettaglio);
	}
	
}
