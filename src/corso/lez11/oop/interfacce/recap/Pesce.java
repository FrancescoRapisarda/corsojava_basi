package corso.lez11.oop.interfacce.recap;

public class Pesce implements Animale{

	private boolean haLeScaglie = true;
	
	@Override
	public void versoEmesso() {
		System.out.println("���");
	}

	@Override
	public void movimento() {
		System.out.println("nuota");
	}

	public boolean isHaLeScaglie() {
		return haLeScaglie;
	}

	public void setHaLeScaglie(boolean haLeScaglie) {
		this.haLeScaglie = haLeScaglie;
	}
	
	

	
	
}
