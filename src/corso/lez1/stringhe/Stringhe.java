package corso.lez1.stringhe;

public class Stringhe {

	public static void main(String[] args) {

//		String frase = "Ciao sono Giovanni";				//Variabile Stringa (sequenza di caratteri)
//		System.out.println(frase);
		
		// -------------------------------------------------- //
		
//		String fraseUno = "Ciao sono";
//		String fraseDue = "Giovanni";
//		
////		String fraseTotale = fraseUno + ": " + fraseDue;
////		System.out.println(fraseTotale);
//		
//		System.out.println(fraseUno + ": " + fraseDue);
		
		// -------------------------------------------------- //

//		int a = 5;
//		int b = 10;
//		String frase = "Ecco il valore";
//		
////		System.out.println(frase + a + b);
////
////		System.out.println(a + b + frase);
//
//		System.out.println(frase + (a + b));
		
		// -------------------------------------------------- //
		
		/*
		 * Giovanni Pace � 10 anni vecchio ed ha una temperatura corporea
		 * di 36.6 gradi.
		 * Mario Rossi � 20 anni vecchio ed ha una temperatura corporea
		 * di 35.8 gradi.
		 * 
		 * VARIABILI: nominativo - eta - temperatura
		 */
		
		String nominativo = "Giovanni Pace";
		int eta = 10;
		float temperatura = 35.8f;
		
		System.out.println(nominativo + " � " + eta + 
				" anni vecchio ed ha una temperatura corporea di " +
				temperatura + " gradi.");
		
		nominativo = "Mario Rossi";
		eta = 20;
		temperatura = 35.8f;
		
		System.out.println(nominativo + " � " + eta + 
				" anni vecchio ed ha una temperatura corporea di " +
				temperatura + " gradi.");
	}

}
