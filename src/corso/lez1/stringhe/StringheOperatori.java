package corso.lez1.stringhe;

public class StringheOperatori {

	public static void main(String[] args) {

//		String nomeUno = "Giovanni";
//		String nomeDue = "GIOVANNI";
//		
//		if(nomeUno.equalsIgnoreCase(nomeDue)) {
//			System.out.println("Stringhe uguali!");
//		}
//		else {
//			System.out.println("Stringhe differenti!");
//		}
		
		// --------------------- COMPARAZIONE ----------------------- //
		
//		String nomeUno = "Giovanni";
//		String nomeDue = "gIoVaNnI";
//		
//		nomeUno = nomeUno.toLowerCase();		//Prendo la vecchia variabile e la metto tutta in minuscolo	
//		nomeDue = nomeDue.toLowerCase();
//		
//		if(nomeUno.equals(nomeDue)) {
//			System.out.println("Stringhe uguali!");
//		}
//		else {
//			System.out.println("Stringhe differenti!");
//		}
		
//		System.out.println(nomeDue.toUpperCase());	//Equivalente in maiuscolo dell'operazione precedente.
		
		// ------------------------- TRIM --------------------------- //
		
//		String nomeUno = " Giovanni Pace";
//		String nomeDue = "gIoVaNnI Pace ";
//		
//		nomeUno = nomeUno.toLowerCase().trim();		//Operazioni sequenziali sulle stringhe
//		nomeDue = nomeDue.toLowerCase().trim();
//		
////		nomeUno = nomeUno.trim();				//Rimuove gli spazi SOLO all'inizio ed alla fine della stringa
////		nomeDue = nomeDue.trim();
//		
//		if(nomeUno.equals(nomeDue)) {
//			System.out.println("Stringhe uguali!");
//		}
//		else {
//			System.out.println("Stringhe differenti!");
//		}
		
		// ---------------------- RICERCA indexOf --------------------------- //
		
//		String frase = "Sono Giovanni e mi piace la programmazione!";
//		
//		//La stringa "Giovanni" � presente nella frase?
//		
////		System.out.println(frase.indexOf("Marco"));		//Restituisce la posizione (partendo da 0) in cui trova la parola
//		
//		if(frase.indexOf("Marco") != -1) {
//			System.out.println("Trovato");
//		}
//		else {
//			System.out.println("NON Trovato");
//		}
		
		// ------------------------------------------------------------------- //
		
////		String nuovaFrase = "Sono Giovanni e mi piace la \"Programmazione ad Oggetti\" in Java";
////		System.out.println(nuovaFrase);
//		
//		String nome = "              ";
//		
////		nome = nome.trim();
////		
////		if(nome.equals("")) {
////			System.out.println("Errore");
////		}
////		else {
////			System.out.println("Ciao " + nome);
////		}
//		
//		if(nome.isBlank()) {	
//			System.out.println("Errore");
//		}
//		else {
//			System.out.println("Ciao " + nome);
//		}
		
		// ----------------------------- length() ----------------------------------- //
		
//		String frase = "Sono Giovanni e mi piace la programmazione!";
//		
//		int lunghezza = frase.length();
//		System.out.println(lunghezza);
		
		// -------------------------------------- Trim e length() --------------------------- //
		
		String nome = "X        ";
		
		if(nome.trim().length() < 3) {
			System.out.println("Errore");
		}
		else {
			System.out.println("Ciao " + nome);
		}
		
	}

}
