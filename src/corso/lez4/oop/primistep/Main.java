package corso.lez4.oop.primistep;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Studente stud_1 = new Studente();	//invocazione del costruttore senza parametri
		stud_1.setNome("Giovanni");
		stud_1.setCognome("Pace");
		stud_1.setMatricola(12345);
		stud_1.setCorso("Ing. Inf");
//		stud_1.stampa();
		
		Studente stud_2 = new Studente("Mario", "Rossi", 12346, "Ing. Ele.");
//		stud_2.stampa();
		
		Studente stud_3 = new Studente();
		stud_3.setNome("Valeria");
		stud_3.setCognome("Verdi");
		stud_3.setCorso("Ing Chi.");
//		stud_3.stampa();
		
		stud_3.setMatricola(12347);
//		stud_3.stampa();
		
		ArrayList<Studente> elenco = new ArrayList<Studente>();
		elenco.add(stud_1);
		elenco.add(stud_2);
		elenco.add(stud_3);
		
		for(int i=0; i<elenco.size(); i++) {
			Studente temp = elenco.get(i);
			temp.stampa();
		}
		
	}

}
