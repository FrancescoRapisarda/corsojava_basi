package corso.lez8.oop.contdinamici.scarpe;

public class Init {

	public static void main(String[] args) {

//		Scarpa nike = new Scarpa("NikeBasket", "Nike", 42.0f, 0, null, 0);
//		System.out.println(nike);
//		
////		Etichetta et1 = nike.getEtiScarpa();
////		et1.setScaffale("A");
////		et1.setPosizione(2);
////		
////		
//		nike.getEtiScarpa().setScaffale("A");
//		nike.getEtiScarpa().setPosizione(2);	
//		System.out.println(nike);
//		
//		nike.getEtiScarpa().setScaffale(null);
//		nike.getEtiScarpa().setPosizione(0);		
//		System.out.println(nike);
		
		// ------------------------------------------------------------ //
		
		
		Negozio bata = new Negozio("Bata");
		Scarpa converse = new Scarpa("ConverseCostosa", "Converse", 42.0f, 0, "A", 2);
		converse.setNomeModello("ConverseBellissima");
		converse.setQuantita(10);
		
		bata.aggiungiScarpa(converse);
		bata.stampaElenco();
		System.out.println("Il totale del negozio " + bata.getNomeNegozio() + " �: " + bata.totaleGiacenza());

		//------
		
		Negozio pittarello = new Negozio("Pittarello");
		
		Scarpa nike = new Scarpa("NikeBasket", "Nike", 42.0f, 20, "A", 2);
		Scarpa superga = new Scarpa("SupergaTennis", "Superga", 42.0f, 30, "A", 2);

		pittarello.aggiungiScarpa(superga);
		pittarello.aggiungiScarpa(nike);
//		pittarello.aggiungiScarpa(converse);	//ATTENZIONE, sto utilizzando lo stesso oggetto presente in BATA, se lo modifico in BATA lo modifico anche qui!
		pittarello.stampaElenco();
		
		int totPittarello = pittarello.totaleGiacenza();
		System.out.println("Il totale del negozio " + pittarello.getNomeNegozio() + " �: " + totPittarello);
		
	}

}
