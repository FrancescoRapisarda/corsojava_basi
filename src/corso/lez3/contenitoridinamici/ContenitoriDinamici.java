package corso.lez3.contenitoridinamici;

import java.util.ArrayList;
import java.util.Scanner;

public class ContenitoriDinamici {

	public static void main(String[] args) {

		
////		Dichiara una variabile chiamata "rubrica" che � un ArrayList di Stringhe
////		In questa variabile inserisci un nuovo ArrayList di Stringhe
//		ArrayList<String> rubrica = new ArrayList<String>();
//
//		rubrica.add("Giovanni Pace");		//Indice 0
//		rubrica.add("Mario Rossi");			//Indice 1
//		rubrica.add("Valeria Verdi");		//Indice 2
//		rubrica.add("Marika Viola");		//Indice 3
//		
////		System.out.println( rubrica.get(0) );
////		System.out.println( rubrica.get(1) );
////		System.out.println( rubrica.get(2) );
////		System.out.println( rubrica.get(3) );
//		
//		rubrica.remove(2);
//		
//		for(int i=0; i<rubrica.size(); i++) {
//			System.out.println( rubrica.get(i) );
//		}
		
		// -------------------------------------------------------------------------- //
		
		/*
		 * Creare un sistema che prenda in input i nomi delle persone e, digitando "Q" esca
		 * dall'inserimento e stampi l'elenco delle persone.
		 */
//		
//		Scanner interceptor = new Scanner(System.in);
//		boolean inserimentoConsentito = true;
//		
//		ArrayList<String> rubrica = new ArrayList<String>();
//		
//		while(inserimentoConsentito) {					//Equivala a scrivere 		while(inserimentoConsentito == true) {
//			
//			System.out.println("Inserisci il nome o digita \"Q\" per uscire:");
//			String input = interceptor.nextLine();
//		
//			if(input.equals("Q")) {
//				inserimentoConsentito = false;
//			}
//			else {
//				//Inserisco l'input nella rubrica
//				rubrica.add(input);
//			}
//		}
//		
//		//Stampo la rubrica
//		System.out.println("----------- RUBRICA -----------");
//		for(int i=0; i<rubrica.size(); i++) {
//			System.out.println( rubrica.get(i) );
//		}
		
		/*
		 * Creare un sistema di gestione libri che prenda in input tutti i titoli dei libri che avete a disposizione.
		 */
		
		Scanner sistemaInput = new Scanner(System.in);
		ArrayList<String> elencoLibri = new ArrayList<String>();
		
		boolean inserimentoAbilitato = true;
		
		while(inserimentoAbilitato) {
			
			System.out.println("Dammi il titolo del libro o digita QUIT:");
			String inputUtente = sistemaInput.nextLine();
			
			if(!inputUtente.equals("Q")) {
				
				if(!inputUtente.isBlank()) {						//Valido l'input ed evito di inserire titoli vuoti!
					elencoLibri.add(inputUtente);
				}
				else {
					System.out.println("Non puoi inserire un titolo vuoto! -.-");
				}
				
			}
			else {
				inserimentoAbilitato = false;
			}
			
		}
		
		System.out.println("Il tuo elenco libri �: ");
		for(int i=0; i<elencoLibri.size(); i++) {
			System.out.println( elencoLibri.get(i) );
		}
		
	}

}
