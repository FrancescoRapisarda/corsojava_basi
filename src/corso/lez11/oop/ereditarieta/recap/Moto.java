package corso.lez11.oop.ereditarieta.recap;

public class Moto extends Veicolo{

	private int numRuote = 2;

	public Moto(int numRuote) {
		super();
		this.numRuote = numRuote;
	}
	
	public Moto(String varTelaio, String varProprietario, String varTarga, int NumRuote) {
		super.telaio = varTelaio;
		super.proprietario = varProprietario;
		super.targa = varTarga;
		this.numRuote = numRuote;
	}
	
	public String stampaDettaglio() {
		
		String dettaglio = "I dettagli del VEICOLO sono:\n"
				+ "Proprietario: " + this.proprietario + "\n"
				+ "Targa: " + this.targa + "\n"
				+ "Telaio: " + this.telaio + "\n"
				+ "Num. Ruote: " + this.numRuote;
		
		return dettaglio;
		
	}

	@Override
	public void accendi() {
		
	}
	
	
	
	
}
