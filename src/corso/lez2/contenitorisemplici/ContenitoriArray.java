package corso.lez2.contenitorisemplici;

import java.util.Scanner;

public class ContenitoriArray {

	public static void main(String[] args) {

//		String[] elencoAutomobili = new String[5];	//Dichiarazione dell'Array
//		
//		elencoAutomobili[0] = "BMW";
//		elencoAutomobili[1] = "FIAT";
//		elencoAutomobili[2] = "Maserati";
//		elencoAutomobili[3] = "Lamborghini";
//		elencoAutomobili[4] = "Lancia";
//		
//		System.out.println(elencoAutomobili[3]);
//		System.out.println(elencoAutomobili[4]);
		
		// ------------------------------------------------------- //
		
		String[] elencoMoto = { "Ducati", "BMW", "Honda", "Piaggio" };
		
//		System.out.println(elencoMoto[15]);
		
//		int indiceAttuale = 0;
//		
//		while(indiceAttuale < elencoMoto.length) {
//			System.out.println(elencoMoto[indiceAttuale]);
//			
//			indiceAttuale++;
//		}
		
//		int indiceAttuale = 0;
//		
//		do {
//			System.out.println(elencoMoto[indiceAttuale]);
//			
//			indiceAttuale++;
//		} while(indiceAttuale < elencoMoto.length);

		
		// ---------------------------------------------- //
		/*
		 
		 for(step_iniziale; condizione; incremento){
		 	blocco_di_codice
		 }

		 */
		
//		String[] invitati = { "Giovanni", "Mario", "Marco", "Valeria", "Erika" };
//		
////		for(int indiceAttuale = 0; indiceAttuale < invitati.length; indiceAttuale++) {
////			System.out.println(invitati[indiceAttuale]);
////		}
//		
//		for(int i=0; i < invitati.length; i++) {
//			System.out.println(invitati[i]);
//		}
		
		/*
		 * Costruire un array di citt� e ciclarlo tramite While - Do While - For
		 */
		
		String[] citta = { "Roma", "Milano", "Bologna", "Taranto", "Pescara" };
		
//		for(int i=0; i<citta.length; i++) {
//			System.out.println(citta[i]);
//		}
//		
//		int i=0;
//		while(i<citta.length) {
//			System.out.println(citta[i]);
//			
//			i++;
//		}
//		
//		i=0;
//		do {
//			System.out.println(citta[i]);
//			i++;
//		} while(i<citta.length);
		
		// ----------------------------------------------------------- //
		
//		Scanner interceptor = new Scanner(System.in);
//		String[] animali = { "Cane", "Gatto", "Babuino", "Lontra", "Carpa", "Tardigrado", "Lontra" };
//		
////		String ricercato = "Cane";
//		System.out.println("Inserisci l'animale da ricercare:");
//		String ricercato = interceptor.nextLine();
//		
//		int contatore = 0;
//		
//		for(int i=0; i<animali.length; i++) {
//			
//			if(animali[i].equals(ricercato)) {
////				System.out.println(ricercato + " tovato!");
//				
//				contatore++;
//			}
//			
//		}
//		
//		System.out.println("Ho incontrato l'animale " + ricercato + " per " + contatore + " volte.");

		// ---------------------------------------------------------- //
		
//		float[] sequenza = { 8, 90, 15, 74, 65, 41 };
//		
//		float somma = 0;
//		
//		for(int i=0; i<sequenza.length; i++) {
//			
//			float temporaneo = sequenza[i];
//			somma += temporaneo;
////			System.out.println(temporaneo);		//Equivalente di: System.out.println(estrazioni[i]);
//			
//		}
//		
//		float media = somma / sequenza.length;
//		
//		System.out.println("Il valore medio della sequenza �:" + media);
		
		// ------------------------------------------------------- //
		
		String[] elencoAutomobili = new String[5];	//Dichiarazione dell'Array
		
		elencoAutomobili[0] = "BMW";
		elencoAutomobili[1] = "FIAT";
//		elencoAutomobili[2] = "Maserati";
		elencoAutomobili[3] = "Lamborghini";
		elencoAutomobili[4] = "Lancia";
		
		for(int i=0; i<elencoAutomobili.length; i++) {
			System.out.println(elencoAutomobili[i]);
		}

//		String nome = null;
//		
//		nome = "Giovanni";
//		
//		nome = null;
//		System.out.println(nome);
		
		
		
	}

}
