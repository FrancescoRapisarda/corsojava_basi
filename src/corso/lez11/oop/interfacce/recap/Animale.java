package corso.lez11.oop.interfacce.recap;

public interface Animale {

	void versoEmesso();
	void movimento();
	
}
