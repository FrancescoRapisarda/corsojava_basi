package corso.lez11.oop.statiche;

public class Studente {

	/*
	 * Attributo di classe statico che viene aggiornato solo da questa 
	 * classe e dall'esterno pu� essere letto solo tramite il metodo 
	 * getContatore anch'esso statico.
	 */
	private static int contatore = 0;
	
	private String nome;
	private String cognome;
	private String matricola;
	
	public Studente() {
		contatore++;
	}
	
	public Studente(String nome, String cognome, String matricola) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.matricola = matricola;
		contatore++;
	}
	
	/*
	 * Metodo statico che permette la lettura all'esterno della variabile
	 * statica contatore
	 */
	public static int getContatore() {
		return contatore;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	
	
}
