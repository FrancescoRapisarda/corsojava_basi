package corso.lez6.oop.primistep.recap;

public class Main {

	public static void main(String[] args) {

		Persona perUno = new Persona();
		Persona perDue = new Persona();
		
		Persona perTre = new Persona("Giovanni", "Pace");
		Persona perQuattro = new Persona("Mario", "Rossi");
		
		Persona perCinque = new Persona("Valeria", "Verdi", 34);
		
		Persona perSei = new Persona(23, "Marika", "Viola");
		perSei.setNome("Leopoldo");
		perSei.stampaPersona();
		
		//perUno = null; //Rende monco il puntatore alla variabile e la sua cella di memoria pu� essere liberata!

	}

}
