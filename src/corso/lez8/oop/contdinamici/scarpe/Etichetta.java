package corso.lez8.oop.contdinamici.scarpe;

public class Etichetta {

	private String codiceEtichetta;	//TODO: Auto generazione
	private String scaffale = "N.D.";
	private int posizione;
	
	public Etichetta() {
		
	}
	
	public Etichetta(String codiceEtichetta, String scaffale, int posizione) {
		this.codiceEtichetta = codiceEtichetta;
		this.setScaffale(scaffale);					//Uso il setScaffale che controlla che l'input non sia Null!
		this.setPosizione(posizione);
	}

	public String getCodiceEtichetta() {
		return codiceEtichetta;
	}

	//Evito che in futuro il codice venga cambiato!
//	public void setCodiceEtichetta(String codiceEtichetta) {
//		this.codiceEtichetta = codiceEtichetta;
//	}

	public String getScaffale() {
		return scaffale;
	}

	public void setScaffale(String scaffale) {
		if(scaffale == null) {
			this.scaffale = "N.D.";
		}
		else {
			this.scaffale = scaffale;
		}
	}

	public int getPosizione() {
		return posizione;
	}

	public void setPosizione(int posizione) {
		this.posizione = posizione;
	}
	
	/**
	 * Accessibile in maniera diretta e scontata direttamente 
	 * dalla SYSO
	 */
	public String toString() {
		String risultato = "CODICE: " + this.codiceEtichetta + " POSIZIONE: " + this.posizione + "-" + this.scaffale;
		return risultato;
	}
	
}
