package corso.lez6.oop.utilizzo.hw;

public class CartaIdentita {

	private String codice;
	private String dataScadenza;
	private String dataEmissione;
	private String luogoEmissione;
	
	public CartaIdentita() {
		
	}
	
	public CartaIdentita(String varCodice, String varDataScad, String varDataEmi, String varLuogoEmi) {
		this.codice = varCodice;
		this.dataScadenza = varDataScad;
		this.dataEmissione = varDataEmi;
		this.luogoEmissione = varLuogoEmi;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDataScadenza() {
		return dataScadenza;
	}

	public void setDataScadenza(String dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	public String getDataEmissione() {
		return dataEmissione;
	}

	public void setDataEmissione(String dataEmissione) {
		this.dataEmissione = dataEmissione;
	}

	public String getLuogoEmissione() {
		return luogoEmissione;
	}

	public void setLuogoEmissione(String luogoEmissione) {
		this.luogoEmissione = luogoEmissione;
	}
	
	
	
	
}
