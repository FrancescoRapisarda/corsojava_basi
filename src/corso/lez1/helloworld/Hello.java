package corso.lez1.helloworld;

public class Hello {

	public static void main(String[] args) {

		//Stampo in console e poi vado a capo
//		System.out.println("Hello World");
//		System.out.println("Hello World");
//		System.out.println("Hello World");
		
		/*
		 * Stampo in console in maniera
		 * sequenziale senza andare mai
		 * a capo!
		 */
		//System.out.print("Ciao sono ");
		//System.out.print("Giovanni");
		
		//Commento una grande porzione di codice
		/*
		System.out.println("Hello World");
		System.out.println("Hello World");
		*/

		/*
		 * Per commentare/decommentare rapidamente il codice posso
		 * utilizzare la scorciatoia Ctrl + Maiusc + 7
		 */
		System.out.print("Ciao sono ");
		System.out.println("Giovanni Pace");
		System.out.print("Ciao sono ");
		System.out.println("Mario Rossi");
	}

}
