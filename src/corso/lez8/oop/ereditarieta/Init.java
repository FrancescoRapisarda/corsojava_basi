package corso.lez8.oop.ereditarieta;

import java.util.ArrayList;
import java.util.List;

public class Init {

	public static void main(String[] args) {

		Persona gio = new Persona("Giovanni", "Pace", "123456", "info@info.com");
//		System.out.println(gio.toString());
		
		Studente mar = new Studente("Mario", "Rossi", "987654", "ciao@ciao.com", "MAT123", 2021, "Chimica");
//		System.out.println(mar.toString());
		
		Manutentore val = new Manutentore();
		val.setNome("Valerio");
		val.setCognome("Rossi");
//		System.out.println(val.toString());

		List<Persona> elencoPersone = new ArrayList<Persona>();
		elencoPersone.add(mar);
		elencoPersone.add(val);
		elencoPersone.add(gio);
		
		for(int i=0; i<elencoPersone.size(); i++) {
			Persona temp = elencoPersone.get(i);
			System.out.println(temp.toString());
		}
		
	}

}
