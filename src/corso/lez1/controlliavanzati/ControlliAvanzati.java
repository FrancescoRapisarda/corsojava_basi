package corso.lez1.controlliavanzati;

public class ControlliAvanzati {

	public static void main(String[] args) {

		/*
		 * Creare un sistema di traduzione della sigla provincia 
		 * in nome effettivo.
		 */
		
//		String sigla = "MI";
//		
//		if( sigla.equals("AQ") ) {
//			System.out.println("L'Aquila");
//		}
//		else {
//			if( sigla.equals("BO") ) {
//				System.out.println("Bologna");
//			}
//			else {
//				if( sigla.equals("MI") ) {
//					System.out.println("Milano");
//				}
//				else {
//					System.out.println("Non la conosco!");
//				}
//			}
//		}
		
		// ---------------------- IF ELSE IF ---------------------- //

//		String sigla = "AQ";
//		
//		if( sigla.equals("AQ") ) {
//			System.out.println("L'Aquila");
//		} else if( sigla.equals("BO") ) {
//			System.out.println("Bologna");
//		} else if( sigla.equals("MI") ) {
//			System.out.println("Milano");
//		} else {
//			System.out.println("Non la conosco!");
//		}
//		
		// ---------------------- IF ---------------------- //
		
//		String sigla = "AQ";
//		
//		if( sigla.equals("AQ") ) {
//			System.out.println("L'Aquila");
//		} 
//		if( sigla.equals("BO") ) {
//			System.out.println("Bologna");
//		} 
//		if( sigla.equals("MI") ) {
//			System.out.println("Milano");
//		}
		
		// --------------------- SWITCH ---------------------//
		
		String sigla = "RM";
		
		switch(sigla) {
			case "AQ":
				System.out.println("L'Aquila");
				break;
			case "BO":
				System.out.println("Bologna");
				break;
			case "MI":
				System.out.println("Milano");
				break;
			default:
				System.out.println("Non la conosco!");
		}
		
		/**
		 * CON SWITCH-CASE
		 * Scrivere un piccolo programma che, dato in input il giorno della settimana (in numero), vi restituisca il nome
		 * - 1 = Luned�
		 * - 2 = Marted�
		 * ...
		 * 
		 * NON RISCRIVENDO LO SWITCH-CASE ;)
		 * CHALLENGE... e se volessimo cambiare il sistema di conversione dei giorni nello stile americano?
		 * - 1 = Domenica
		 * - 2 = Luned�
		 * - 3 = Marted�
		 * ...
		 */

		
	}

}
