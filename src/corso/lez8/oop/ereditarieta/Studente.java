package corso.lez8.oop.ereditarieta;

public class Studente extends Persona{

	private String matricola;
	private int annoImm;
	private String corsoIsc;
	
	public Studente() {
		
	}
	
	public Studente(String nome, String cognome, String telefono, String email, String matricola, int annoImm, String corsoIsc) {
		super.nome = nome;
		super.cognome = cognome;
		super.telefono = telefono;
		super.email = email;
		this.matricola = matricola;
		this.annoImm = annoImm;
		this.corsoIsc = corsoIsc;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public int getAnnoImm() {
		return annoImm;
	}

	public void setAnnoImm(int annoImm) {
		this.annoImm = annoImm;
	}

	public String getCorsoIsc() {
		return corsoIsc;
	}

	public void setCorsoIsc(String corsoIsc) {
		this.corsoIsc = corsoIsc;
	}

	@Override
	public String toString() {
		return "Studente [matricola=" + matricola + ", annoImm=" + annoImm + ", corsoIsc=" + corsoIsc + ", nome=" + nome
				+ ", cognome=" + cognome + ", telefono=" + telefono + ", email=" + email + "]";
	}
	
	
	
	
	
}
